import jwt from 'jsonwebtoken';
import { merge } from 'lodash';
import { GraphQLServer } from 'graphql-yoga';
import {
  typeDef as typeUser,
  resolvers as resolversUser,
} from './models.refactor/user.model';

import {
  typeDef as typePost,
  resolvers as resolversPost,
} from './models.refactor/post.model';

const typeDefs = `
  enum CategoryName {
    film,
    sport,
  }

  type Category {
    name: CategoryName
    color: String,
  }

  type Auth {
    token: String!
  }
  type Query {
    category: Category
  }

  input CategoryInput {
    name: CategoryName
  }
  type Mutation {
    _empty: String
  }
`;

const resolvers = {
  Query: {
    category() {
      return {
        name: 'film',
      };
    },
  },
};


const server = new GraphQLServer({
  typeDefs: [typeDefs, typeUser, typePost],
  resolvers: merge(resolvers, resolversUser, resolversPost),
  context(request) {
    return {
      request,
      getAuthUser() {
        const token = request.request.headers.auth;
        if (!token) {
          throw new Error('Failed authentication');
        }
        const user = jwt.verify(token, 'password');
        if (!user) {
          throw new Error('Failed authentication');
        }
        return user;
      },
    };
  },
});
server.start(() => {
  console.log('Start');
});
