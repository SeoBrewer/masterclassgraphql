import jwt from 'jsonwebtoken';
import { GraphQLServer } from 'graphql-yoga';
import {
  typeUser,
  queryUser,
  mutationUser,
  resolveQueryUser,
  resolveMutationUser,
} from './models/user.model';

import {
  typePost,
  queryPost,
  mutationPost,
  resolveQueryPost,
  resolveMutationPost,
  resolveAddedPost,
} from './models/post.model';

const typeDefs = `
  ${typeUser}
  ${typePost}

  type Auth {
    token: String!
  }
  type Query {
    ${queryUser}
    ${queryPost}
  }

  type Mutation {
    ${mutationUser}
    ${mutationPost}
  }
`;

const resolvers = {
  Query: Object.assign({}, resolveQueryUser, resolveQueryPost),
  ...resolveAddedPost,
  Mutation: {
    ...resolveMutationUser,
    ...resolveMutationPost,
  },
};


const server = new GraphQLServer({
  typeDefs,
  resolvers,
  context(request) {
    return {
      request,
      getAuthUser() {
        const token = request.request.headers.auth;
        if (!token) {
          throw new Error('Failed authentication');
        }
        const user = jwt.verify(token, 'password');
        if (!user) {
          throw new Error('Failed authentication');
        }
        return user;
      },
    };
  },
});
server.start(() => {
  console.log('Start');
});
