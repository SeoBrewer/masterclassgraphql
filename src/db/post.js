import shortid from 'shortid';
import db from './db';

const POSTS = 'posts';

function getPost(id) {
  return db.get(POSTS)
    .find({ id })
    .value();
}

function getPostsOfUser(id) {
  return db.get(POSTS)
    .filter({ author: id })
    .value();
}

function addPost(post) {
  const id = shortid.generate();
  const postSave = Object.assign({}, post, { id });
  db.get(POSTS)
    .push(postSave)
    .write();
  return getPost(id);
}

export {
  getPost,
  addPost,
  getPostsOfUser,
};
