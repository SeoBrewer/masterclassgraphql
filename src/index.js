import jwt from 'jsonwebtoken';
import { GraphQLServer } from 'graphql-yoga';
import { getUser, addUser } from './db/user';
import { getPost, addPost, getPostsOfUser } from './db/post';

const typeDefs = `
  type User {
    id: ID!
    name: String!,
    category: Category
  }
  type Post {
    id: ID!
    title: String!
    text: String
    avatar: String
    author: User!
  }

  enum CategoryName {
    film,
    sport,
  }

  type Category {
    name: CategoryName
    color: String,
  }

  type Auth {
    token: String!
  }
  type Query {
    user(id: ID!): User!
    post(id: ID!): Post
    me: User!
    myposts: [Post]!
    category: Category
  }

  input CategoryInput {
    name: CategoryName
  }

  type Mutation {
    createUser(name: String!, category: CategoryInput!): Auth!
    createPost(title: String!, text: String, avatar: String, author: ID!): Post!
  }
`;

const resolvers = {
  Query: {
    user(_, { id }) {
      return getUser(id);
    },
    post(_, { id }) {
      return getPost(id);
    },
    me(_, obj, { getAuthUser }) {
      const user = getAuthUser();
      return getUser(user.id);
    },
    myposts(_, obj, { getAuthUser }) {
      const user = getAuthUser();
      return getPostsOfUser(user.id);
    },
    category() {
      return {
        name: 'film',
      };
    },
  },
  Post: {
    author(parent) {
      return getUser(parent.author);
    },
  },
  User: {
    category(parent) {
      return {
        ...parent.category,
        color: 'red',
      };
    },
  },
  Mutation: {
    createUser(_, user) {
      return addUser(user);
    },
    createPost(_, post) {
      const user = getUser(post.author);
      if (!user) {
        throw new Error('Author no exists');
      }
      return addPost(post);
    },
  },
};


const server = new GraphQLServer({
  typeDefs,
  resolvers,
  context(request) {
    return {
      request,
      getAuthUser() {
        const token = request.request.headers.auth;
        if (!token) {
          throw new Error('Failed authentication');
        }
        const user = jwt.verify(token, 'password');
        if (!user) {
          throw new Error('Failed authentication');
        }
        return user;
      },
    };
  },
});
server.start(() => {
  console.log('Start');
});
