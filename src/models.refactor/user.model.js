import { getUser, addUser } from '../db/user';

export const typeDef = `
  extend type Query {
    user(id: ID!): User!
    me: User!
  }
  extend type Mutation {
    createUser(name: String!, category: CategoryInput!): Auth!
  }
  type User {
    id: ID!
    name: String!,
    category: Category
  }
`;
export const resolvers = {
  Query: {
    user(_, { id }) {
      return getUser(id);
    },
    me(_, obj, { getAuthUser }) {
      const user = getAuthUser();
      return getUser(user.id);
    },
  },
  User: {
    category(parent) {
      return {
        ...parent.category,
        color: 'red',
      };
    },
  },
  Mutation: {
    createUser(_, user) {
      return addUser(user);
    },
  },
};
