import { getUser } from '../db/user';
import { getPost, addPost, getPostsOfUser } from '../db/post';

export const typeDef = `
  extend type Query {
    post(id: ID!): Post
    myposts: [Post]!
  }
  extend type Mutation {
    createPost(title: String!, text: String, avatar: String, author: ID!): Post!
  }
  type Post {
    id: ID!
    title: String!
    text: String
    avatar: String
    author: User!
  }
`;

export const resolvers = {
  Query: {
    post(_, { id }) {
      return getPost(id);
    },
    myposts(_, obj, { getAuthUser }) {
      const user = getAuthUser();
      return getPostsOfUser(user.id);
    },
  },
  Post: {
    author(parent) {
      return getUser(parent.author);
    },
  },
  Mutation: {
    createPost(_, post) {
      const user = getUser(post.author);
      if (!user) {
        throw new Error('Author no exists');
      }
      return addPost(post);
    },
  },
};
