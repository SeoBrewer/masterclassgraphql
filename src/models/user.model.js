import { getUser, addUser } from '../db/user';

const typeUser = `
  type User {
    id: ID!
    name: String!,
  }
`;

const queryUser = `
  me: User!
  user(id: ID!): User!
`;

const mutationUser = `
  createUser(name: String!): Auth!
`;

const resolveQueryUser = {
  user(_, { id }) {
    return getUser(id);
  },
  me(_, obj, { getAuthUser }) {
    const user = getAuthUser();
    return getUser(user.id);
  },
};

const resolveMutationUser = {
  createUser(_, user) {
    return addUser(user);
  },
};

export {
  typeUser,
  queryUser,
  mutationUser,
  resolveQueryUser,
  resolveMutationUser,
};
